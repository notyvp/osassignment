//============================================================================
// Name        : SortST.cpp
// Author      : Chea Sovannoty
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <time.h>
using namespace std;
// C program for implementation of Bubble sort
#include <stdio.h>

void swap(int *xp, int *yp) {
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

// A function to implement bubble sort
void bubbleSort(int arr[], int n) {
	int i, j;
	for (i = 0; i < n - 1; i++)

		// Last i elements are already in place
		for (j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j + 1])
				swap(&arr[j], &arr[j + 1]);
}

/* Function to print an array */
void printArray(int arr[], int size) {
	int i;
	for (i = 0; i < size; i++)
		printf("%d ", arr[i]);

}

// Driver program to test above functions
int main() {
	int arr[] = { 64,17,34,18,4,7,8,13, 34, 25, 12, 22, 11, 90 ,7,32,12,54,2,1,67,6,61,5,3,23,9,58};
	int n = sizeof(arr) / sizeof(arr[0]);
	clock_t t;
	int f;
	t = clock();
	bubbleSort(arr, n);
	t = clock() - t;
	printf("It took me %d clicks (%f seconds).\n", t,
			((float) t) / CLOCKS_PER_SEC);
	printf("Sorted array: \n");
	printArray(arr, n);
	return 0;
}
