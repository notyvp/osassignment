//============================================================================
// Name        : MatrixMT.cpp
// Author      : Chea Sovannoty
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <time.h>
#include <pthread.h>
#include <stdio.h>

#include <chrono>

using namespace std;
using namespace std::chrono;

int r1, c1, r2, c2;
int A[2000][2000], B[2000][2000], C[2000][2000];

void *matrix_multiplication(void *threadid) {
	int i = *((int*) threadid);
	for (int j = 0; j < c2; j++) {
		C[i][j] = 0;
		for (int k = 0; k < r2; k++) {
			C[i][j] += A[i][k] * B[k][j];
		}
	}
	pthread_exit(NULL);
}

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	int i, j;

	cout << "Enter number of rows and columns of matrix A : ";
	cin >> r1 >> c1;

	cout << "Enter number of rows and columns of matrix B : ";
	cin >> r2 >> c2;
	if (c1 != r2) {
		cout << "Matrices cannot be multiplied!";
		exit(0);
	}

	cout << "Enter elements of matrix A : ";
	for (i = 0; i < r1; i++)
		for (j = 0; j < c1; j++)
			A[i][j] = rand() % 100;

	cout << "Enter elements of matrix B : ";
	for (i = 0; i < r2; i++)
		for (j = 0; j < c2; j++)
			B[i][j] = rand() % 100;

	// Get starting timepoint
	auto start = high_resolution_clock::now();

	pthread_t threads[r1];
	int index[r1];

	for (i = 0; i < r1; i++) {
		index[i] = i;
		pthread_create(&threads[i], NULL, matrix_multiplication, &index[i]);
	}
	for (int i = 0; i < r1; i++) {
		pthread_join(threads[i], NULL);

	}
	// Get ending timepoint
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);
	cout << "Time taken by function: " << duration.count() << " microseconds"
			<< endl;

	return 0;

}
